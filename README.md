# Gauss-Jordan Calculator for IF2123
A program to transform a matrix using the Gauss-Jordan Elimination technique.
Written in Java.
Made to fulfill an assignment for Geometric Algebra course at Institut Teknologi Bandung. 

## Author
* Senapati Sang Diwangkara
* [Shevalda Gracielira](https://github.com/shevalda)
* ...
