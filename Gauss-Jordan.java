/* Nama Program	: Kalkulator Gauss-Jordan
 * NIM / Nama anggota 1	: 13516064 / Irfan Ihsanul Amal
 * NIM / Nama anggota 2	: 13516107 / Senapati Sang Diwangkara
 * NIM / Nama anggota 3	: 13516134 / Shevalda Gracielira
 */

import java.util.*;
import java.io.*;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;

class matriks {
	/* --- Atribut --- */
	
	float[][] Mat = new float [101][101];
	int KolEff, BarEff;
	private float delta = 0.00001f;
	String nama_file;
	
	/* --- Method --- */
	
	public void BacaMatriksDariFile() throws IOException
	/* --- Membaca Matriks dari file eksternal --- */
	{     
		InputStream is = null; 
		InputStreamReader isr = null;
		BufferedReader br = null;
		int kol = 1;
		int bar = 1;

		System.out.println("Mulai membaca dari file eksternal...");
		try {
			is = new FileInputStream(nama_file);
			isr = new InputStreamReader(is);
			br = new BufferedReader(isr);
      
			int value;
			
			value = br.read();
			while (value != -1) {
				char c = (char) value;
				
				while (c == ' ' && value != -1) {
					value = br.read();
					c = (char) value;
				}
				
				StringBuilder sb = new StringBuilder();
				String str;
				while (c != ' ' && c != '\n' && value != -1) {
					sb.append(c);
					value = br.read();
					c = (char) value;
				}
				str = sb.toString();
				
				float elmt = Float.parseFloat(str);
				Mat[bar][kol] = elmt;
				if (c == ' ') {
					kol++;
				} else if (c == '\n' && value != -1) {
					bar++;
					kol = 1;
				}
				value = br.read();
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			if(is!=null) is.close();
			if(isr!=null) isr.close();
			if(br!=null) br.close();
		}
		KolEff = kol;
		BarEff = bar;
		System.out.println("Pembacaan dari file eksternal selesai.");
	}
	
	public void BacaMatriks()
	/* Membaca matriks dari dalam program */
	{		
		int i,j;
		Scanner in = new Scanner (System.in);	
	
		for (i=1; i<= BarEff; i++) {
			for (j=1; j <= KolEff; j++) {
				this.Mat[i][j] = in.nextFloat();
			}
		}
	}
	
	public void SwapBaris(int baris1, int baris2)
	/* Menukar satu baris dengan baris lain */
	{
		float temp;
		
		for (int i = 1; i <= KolEff; i++) {
			temp = Mat[baris1][i];
			Mat[baris1][i] = Mat[baris2][i];
			Mat[baris2][i] = temp;
		}
	}
	
	public void ScaleBaris (int baris, float nilai)
	/* Mengkali sebuah baris dengan sebuah nilai skalar */
	{
		for (int i = 1; i <= KolEff; i++) {
			Mat[baris][i] *= nilai;
		}
	}
	
	public void CombineBaris(int barisTarget, int barisAcuan, float nilai)
	/* Mengurangi baris target dengan baris acuan yang dikali sebuah nilai skalar */
	{
		for (int i = 1; i <= KolEff; i++) {
			Mat[barisTarget][i] -= nilai * Mat[barisAcuan][i];
		}
	}
	
	public void Gauss()
	/* Aplikasi Metode Gauss */
	{
		int barCheck = 1;
		int kolCheck = 1;
		int max; // Baris yang elemennya maximum
		int maxCheck; // Baris yang sedang diperiksa
		
		while (kolCheck <= KolEff && barCheck <= BarEff) {
			maxCheck = barCheck;
			max = barCheck;
			while (maxCheck <= BarEff) {
				// find maximum
				if ((-1 * Math.abs(Mat[maxCheck][kolCheck])) > (-1 * Math.abs(Mat[max][kolCheck]))) {max = maxCheck;}
				maxCheck++;
			}	
			// swap dkk
			if (Math.abs(Mat[max][kolCheck]) != 0) {
				// jika 0 semua, kolCheck++
				// jika ada yg tdk 0, kolCheck++, barCheck++
				SwapBaris(barCheck, max);
				ScaleBaris(barCheck, (1/Mat[barCheck][kolCheck]));
				for (int i = barCheck + 1; i <= BarEff; i++) {
					CombineBaris(i, barCheck, Mat[i][kolCheck]);
				}
				barCheck++;
			}
			kolCheck++;
		}
	}
	
	public void Jordan()
	/* Aplikasi Metode Jordan setelah Metode Gauss */
	{
		int barCheck = BarEff;
		int kolCheck;
		int barCombine;
		
		while (barCheck >= 2) {
			kolCheck = 1;
			while ((kolCheck < KolEff) && ((this.Mat[barCheck][kolCheck] > (1 + delta)) || (this.Mat[barCheck][kolCheck] < (1 - delta)))) {kolCheck++;}
			if (kolCheck < KolEff) {
				barCombine = barCheck - 1;
				while (barCombine >= 1) {
					CombineBaris(barCombine, barCheck, Mat[barCombine][kolCheck]);
					barCombine--;
				}
			}
			barCheck--;
		}
	}
	
	public int CekSolusi()
	/* Mencek apakah solusi unik, banyak solusi, tidak ada solusi */
	{
		/* Solusi unik mengembalikan 1
		 * Banyak solusi mengembalikan 2
		 * Tidak ada solusi mengembalikan 3 */
		int i = 1;
		while (i < KolEff - 1 && Mat[BarEff][i] == 0) { i++; }
		if (Mat[BarEff][i] != 0) return 1;		// solusi unik 
		else {
			if (Mat[BarEff][KolEff] == 0) return 2;
			else return 3;
		}
	}
	
	public void TampilanSolusi()
	/* Menampilkan solusi di dalam program */
	{
		String solusi;
		if (CekSolusi() == 3) {		// Tidak ada solusi
			solusi = "Tidak ada solusi";
			System.out.println(solusi);
		}	
		else if (CekSolusi() == 1) {	// Solusi Unik
			int i = 1;
			while (i <= BarEff) {
				solusi = "";
				solusi = solusi + String.format("x%d = %.2f", i, Mat[i][KolEff]);
				System.out.println(solusi);
				i++;
			}
		}
		else if (CekSolusi() == 2) {	// Banyak Solusi
			int temp = BarEff;
			while(CekSolusi() == 2){
				BarEff--;
			}
			for (int i=1; i<=BarEff; i++){
				solusi = "";
				solusi = String.format("x%d = ", FirstOne(i));
				int j = FirstOne(i) + 1;
				while (j<=KolEff-1){
					solusi = solusi + String.format("%.2fp%d + ", -Mat[i][FindNonZero(i,j)], FindNonZero(i,j));
					j = FindNonZero(i,j) + 1;
				}
				solusi = solusi + String.format("%.2f", Mat[i][KolEff]);
				System.out.println(solusi);
			}
			BarEff = temp;
		}
	}
	
	public void TulisSolusiKeFile() throws IOException
	/* Menuliskan solusi ke file eksternal */
	{
		String solusi;
		BufferedWriter out;
		
		try {
			out = new BufferedWriter(new FileWriter(nama_file, true));
			
			solusi = System.lineSeparator() + System.lineSeparator();
			out.write(solusi);
			solusi = "SOLUSI";
			solusi = solusi + System.lineSeparator();
			out.write(solusi);
			
            if (CekSolusi() == 3) {			// Tidak ada solusi
				solusi = "Tidak ada solusi";
				solusi = solusi + System.lineSeparator();
				out.write(solusi);
			}	
			else if (CekSolusi() == 1) {	// Solusi Unik
				int i = 1;
				while (i <= BarEff) {
					solusi = "";
					solusi = solusi + String.format("x%d = %.2f", i, Mat[i][KolEff]);
					solusi = solusi + System.lineSeparator();
					out.write(solusi);
					i++;
				}
			} else if (CekSolusi() == 2) {	// Banyak Solusi
				int temp = BarEff;

				while(CekSolusi() == 2) BarEff--;
				for (int i = 1; i <= BarEff; i++) {
					solusi = "";
					solusi = String.format("x%d = ", FirstOne(i));
					int j = FirstOne(i) + 1;
					while (j<=KolEff-1){
						solusi = solusi + String.format("%.2fp%d + ", -Mat[i][FindNonZero(i,j)], FindNonZero(i,j));
						j = FindNonZero(i,j) + 1;
					}
					solusi = solusi + String.format("%.2f", Mat[i][KolEff]);
					solusi = solusi + System.lineSeparator();
					out.write(solusi);
				}
				BarEff = temp;
			}
			
			out.close();
			System.out.println("Solusi telah ditulis di file eksternal.");
		} catch (IOException e) {
			e.printStackTrace();      
        }
	}
	
	public int FirstOne(int i)
	/* Cek indeks leading one di suatu baris */
	{
		int j = 1;
		while (!((this.Mat[i][j] < 1 + delta) && (this.Mat[i][j] > 1 - delta))){
			j++;
		}
		return j;
	}
	
	public int FindNonZero(int i, int j)
	/* Menghasilkan indeks pertama di mana elemen bukan 0 ditemukan */
	{
		while ((this.Mat[i][j] < 0 + delta) && (this.Mat[i][j] > 0 - delta)){
			j++;
		}
		return j;
	}
}

/* --- Program untuk Sistem Persamaan Linear --- */
class LinEq {
	public static void main() {
		Scanner choice = new Scanner(System.in);
		matriks M = new matriks();
		
		System.out.println("SISTEM PERSAMAAN LINEAR");
		System.out.println("\nPilih jenis input:");
		System.out.println("1. Langsung dari program");
		System.out.println("2. File eksternal");
		System.out.print("Pilihan : ");
		int pilBaca = choice.nextInt();
		
		System.out.println();
		if (pilBaca == 1) {		// baca matriks dari program
			System.out.print("Banyak persamaan : ");
			M.BarEff = choice.nextInt();
        
			System.out.print("Banyak variabel : ");
			M.KolEff = choice.nextInt();
			M.KolEff = M.KolEff + 1;
			
			System.out.println("Mulai input persamaan:");
			M.BacaMatriks();
		} else if (pilBaca == 2) {	// baca matriks dari file eksternal
			System.out.print("Masukan nama file eksternal : ");
			M.nama_file = choice.next();
			try {
				M.BacaMatriksDariFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		M.Gauss();
		M.Jordan();
		
		if (pilBaca == 1) {		// baca matriks dari program
			System.out.println();
			System.out.println("SOLUSI");
			M.TampilanSolusi();
		} else if (pilBaca == 2) {	// baca matriks dari file eksternal
			try {
				M.TulisSolusiKeFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}		
	}
}

/* --- Program untuk Interpolasi --- */
class PolyInterpolation {
	public static void main() {
		Scanner epson = new Scanner(System.in);
		Scanner choice = new Scanner(System.in);
		matriks X = new matriks();
		
		System.out.println("INTERPOLASI");
		System.out.println("Pilih jenis input:");
		System.out.println("1. Langsung dari program");
		System.out.println("2. File eksternal");
		System.out.print("Pilihan : ");
		int pilBaca = choice.nextInt();
		
		System.out.println();
		if (pilBaca == 1) {			// baca matriks dari program
			float x; float fx; int i; int j;
			System.out.print("Degree : ");
			X.KolEff = epson.nextInt() + 2;
			System.out.print("Banyak sampel : ");
			X.BarEff = epson.nextInt();
			System.out.println("Input Sample (f(x) x): "); 
			for (i = 1; i <= X.BarEff; i++) {
				fx = epson.nextFloat();
				x = epson.nextFloat();
			
				for(j = 1; j <= X.KolEff - 1; j++) {
					X.Mat[i][j] = (float) Math.pow(x, j - 1);
				}
				X.Mat[i][X.KolEff] = fx;
			}
		} else if (pilBaca == 2) {	// baca matriks dari file eksternal
			System.out.print("Masukan nama file eksternal : ");
			X.nama_file = choice.next();
			try {
				X.BacaMatriksDariFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		X.Gauss();
		X.Jordan();
		
		if (pilBaca == 1) {		// baca matriks dari program
			System.out.println();
			System.out.println("SOLUSI");
			X.TampilanSolusi();
		} else if (pilBaca == 2) {	// baca matriks dari file eksternal
			try {
				X.TulisSolusiKeFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}		
	}
}

/* --- Menu awal aplikasi Kalkulator Gauss-Jordan --- */
class KalkulatorGaussJordan {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		PolyInterpolation PI = new PolyInterpolation();
		LinEq LE = new LinEq();
		
		System.out.println("SELAMAT DATANG DI KALKULATOR GAUSS-JORDAN\n");
		System.out.println("Pilih jenis masalah yang akan diselesaikan:");
		System.out.println("1. Sistem Persamaan Linear");
		System.out.println("2. Interpolasi");
		System.out.print("Pilihan : ");
		int choice = input.nextInt();
		if (choice == 1) {		// Sistem Persamaan Linier
			LE.main();
		} else if (choice == 2) {	// Interpolasi
			PI.main();
		}
	}
}
